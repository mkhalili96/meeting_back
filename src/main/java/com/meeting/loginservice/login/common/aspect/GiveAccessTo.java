package com.meeting.loginservice.login.common.aspect;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface GiveAccessTo {

    String role();

}
