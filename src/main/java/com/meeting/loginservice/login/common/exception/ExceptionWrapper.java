package com.meeting.loginservice.login.common.exception;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meeting.loginservice.login.common.exception.dto.ErrorDto;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;
import com.meeting.loginservice.login.common.utils.exception.InvalidTokenException;
import com.meeting.loginservice.login.model.serivce.AuthService.FacebookAuthenticator;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionWrapper extends ResponseEntityExceptionHandler {
    private org.slf4j.Logger log = LoggerFactory.getLogger(ExceptionWrapper.class);

//    @Autowired
//    Tracer tracer;

    @Autowired
    ObjectMapper objectMapper;

//    @ExceptionHandler(FeignException.class)
//    public ResponseEntity<ErrorDto> handleFeignExceptions(Exception e, HttpServletRequest request) throws JsonProcessingException {
//
//        ErrorDto error = objectMapper.readValue(((FeignException) e).contentUTF8(), ErrorDto.class);
//
//        return new ResponseEntity(error, HttpStatus.valueOf(((FeignException) e).status()));
//    }

    @ExceptionHandler(ExceptionsTemplate.class)
    public ResponseEntity<ErrorDto> handleAllCustomExceptions(Exception e, HttpServletRequest request) {

        ErrorDto error = new ErrorDto();

        ExceptionsTemplate template = (ExceptionsTemplate) e;
        error.setMessage(template.getMessage());
        error.setError(template.getError());
        error.setStatus(template.getStatus());
//        error.setTraceId(tracer.currentSpan().context().traceIdString());

        return new ResponseEntity(error, ErrorMap.getHttpStatus(template.getStatus()));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> handleOtherExceptions(Exception e, HttpServletRequest request) {

        // error template
        ErrorDto error = new ErrorDto();

        if (e instanceof AccessDeniedException || e instanceof org.springframework.security.access.AccessDeniedException) {
            error.setError(ErrorMap.ACCESS_DENIED);
            error.setStatus(ErrorMap.ACCESS_DENIED.value());
            error.setMessage(e.getMessage());
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        if (e instanceof ObjectOptimisticLockingFailureException) {
            error.setError(ErrorMap.VERSION_NOT_MATCH);
            error.setStatus(ErrorMap.VERSION_NOT_MATCH.value());
            error.setMessage("input version field must be same as current version");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
//        if (e instanceof IllegalArgumentException) {
//            error.setError(ErrorMap.ILLEGAL_ARGUMENT);
//            error.setStatus(ErrorMap.ILLEGAL_ARGUMENT.value());
//            error.setMessage("Illegal Argument");
////            error.setTraceId(tracer.currentSpan().context().traceIdString());
//
//            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
//        }
//        if (e instanceof ExpiredJwtException) {
//            error.setError(ErrorMap.Unauthorized);
//            error.setStatus(ErrorMap.Unauthorized.value());
//            error.setMessage("JWT Token has expired");
////            error.setTraceId(tracer.currentSpan().context().traceIdString());
//
//            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
//        }
        if (e instanceof SignatureException || e instanceof ResponseStatusException) {
            error.setError(ErrorMap.Unauthorized);
            error.setStatus(ErrorMap.Unauthorized.value());
            error.setMessage("Unauthorized Access");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        if (e instanceof IllegalStateException) {
            error.setError(ErrorMap.FORBIDDEN);
            error.setStatus(ErrorMap.FORBIDDEN.value());
            error.setMessage("File must be an Image");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        if (e instanceof JWTVerificationException) {
            error.setError(ErrorMap.Unauthorized);
            error.setStatus(ErrorMap.Unauthorized.value());
            error.setMessage("Unauthorized Access");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        if (e instanceof InternalAuthenticationServiceException || e instanceof BadCredentialsException){
            error.setError(ErrorMap.INVALID_CREDENTIALS);
            error.setStatus(ErrorMap.INVALID_CREDENTIALS.value());
            error.setMessage("username or password is incorrect ..");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        if (e instanceof InvalidTokenException){
            error.setError(ErrorMap.INVALID_CREDENTIALS);
            error.setStatus(ErrorMap.INVALID_CREDENTIALS.value());
            error.setMessage("idToken is invalid");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());

            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        // if an unexpected exception happened
        else {
            error.setStatus(ErrorMap.INTERNAL_SERVER_ERROR.value());
            error.setError(ErrorMap.INTERNAL_SERVER_ERROR);
            log.info("exception happened : " + e.getMessage());
            error.setMessage("ERROR");
//            error.setTraceId(tracer.currentSpan().context().traceIdString());
            e.printStackTrace();

            return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorDto error = new ErrorDto();

        error.setStatus(ErrorMap.INPUT_VALIDATION_FAILED.value());
        error.setError(ErrorMap.INPUT_VALIDATION_FAILED);

        Map<String, String> errors = new HashMap<String, String>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }

        error.setMessage((Map<String, String>) errors);

        return new ResponseEntity<>(error, headers, status);
    }

}