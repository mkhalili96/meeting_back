package com.meeting.loginservice.login.common.exception.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ErrorDto implements Serializable {
    private Date timestamp = new Date();

    private int status;

    private ErrorMap error;

    private Object message;

    private String traceId;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ErrorMap getError() {
        return error;
    }

    public void setError(ErrorMap error) {
        this.error = error;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public ErrorDto() {
    }

    public ErrorDto(Date timestamp, int status, ErrorMap error, Object message, String traceId) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.traceId = traceId;
    }
}
