package com.meeting.loginservice.login.common.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meeting.loginservice.login.common.exception.dto.ErrorDto;
import com.meeting.loginservice.login.common.exception.dto.exceptiontemplate.JwtExpiredException;
import com.meeting.loginservice.login.common.exception.dto.exceptiontemplate.UserNotFoundException;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {


    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider tokenProvider;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {

        try {
            String header = request.getHeader(Properties.HEADER_STRING);
            if (header == null || !header.startsWith(Properties.TOKEN_PREFIX)) {

                chain.doFilter(request, response);
                return;
            }
            Authentication authentication = getUsernamePasswordAuthentication(request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
        } catch (ServletException e) {
            resolver.resolveException(request, response, null, e);
        } catch (IOException e) {
            resolver.resolveException(request, response, null, e);
        } catch (JwtExpiredException e) {
            resolver.resolveException(request, response, null, e);
        }catch (Exception e){
            resolver.resolveException(request, response, null, e);
        }
    }

    private Authentication getUsernamePasswordAuthentication(HttpServletRequest request) throws JwtExpiredException {
        String token = request.getHeader(Properties.HEADER_STRING)
                .replace(Properties.TOKEN_PREFIX, "");

        if (StringUtils.hasText(token) && tokenProvider.validateToken(token)) {
            String userId = tokenProvider.getUserNameFromJWT(token);

            if (userId != null) {
                Optional<User> user = userRepository.findById( userId);
                if (user.isPresent()) {
                    UserPrincipal principal = new UserPrincipal(user.get());
                    return new UsernamePasswordAuthenticationToken(principal, null, principal.getAuthorities());
                }
            }
            return null;
        }
        return null;
    }
}
