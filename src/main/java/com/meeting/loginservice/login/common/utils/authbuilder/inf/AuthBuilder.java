package com.meeting.loginservice.login.common.utils.authbuilder.inf;

import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.to.CredentialRequestModel;
import com.meeting.loginservice.login.to.LoginResponse;
import org.springframework.security.core.Authentication;
import javax.servlet.http.HttpServletRequest;

public interface AuthBuilder {

    default LoginResponse doAuthentication(CredentialRequestModel credentialRequestModel, HttpServletRequest request) throws Exception{
        Authentication authentication = authenticate(credentialRequestModel,request);
        UserPrincipal userPrincipal = getPrincipal(authentication);
        checkIsPasswordExpired(userPrincipal);
        userIsVerified(userPrincipal);
        generateAuthToken(userPrincipal,request);
        LoginResponse token = new LoginResponse(generateJwtToken(userPrincipal));
        return token;
    }

    Authentication authenticate (CredentialRequestModel credentialRequestModel, HttpServletRequest request) throws Exception;

    UserPrincipal getPrincipal(Authentication authentication) throws Exception;

    void generateAuthToken(UserPrincipal userPrincipal , HttpServletRequest request) throws Exception;

    String generateJwtToken(UserPrincipal userPrincipal) throws Exception;

    void checkIsPasswordExpired(UserPrincipal principal) throws Exception;

    void userIsVerified(UserPrincipal userPrincipal) throws Exception;
}
