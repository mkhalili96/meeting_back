package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class InvalidVerificationRequest extends ExceptionsTemplate {


    public InvalidVerificationRequest() {
        super("user already exist, use forget password");
    }

    @Override
    public int getStatus() {
        return ErrorMap.INVALID_VERIFICATION_REQUEST.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.INVALID_VERIFICATION_REQUEST;
    }
}
