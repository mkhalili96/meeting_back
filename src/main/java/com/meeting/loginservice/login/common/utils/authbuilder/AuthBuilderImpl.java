package com.meeting.loginservice.login.common.utils.authbuilder;


import com.meeting.loginservice.login.common.config.Properties;
import com.meeting.loginservice.login.common.utils.authbuilder.inf.AuthBuilder;
import com.meeting.loginservice.login.model.entity.RegisterStateEnum;
import com.meeting.loginservice.login.model.entity.Role;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.to.CredentialRequestModel;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;

@Component
public class AuthBuilderImpl implements AuthBuilder {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;


    @Override
    public Authentication authenticate(CredentialRequestModel credentialRequestModel, HttpServletRequest request) throws Exception {
        // AUTHENTICATE USER
        return authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        credentialRequestModel.getUniqueIdentity(),
                        credentialRequestModel.getPassword())
                );
    }

    @Override
    public UserPrincipal getPrincipal(Authentication authentication) throws Exception {
        return (UserPrincipal) authentication.getPrincipal();
    }

    @Override
    public void generateAuthToken(UserPrincipal userPrincipal, HttpServletRequest request) throws Exception {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                userPrincipal, null, userPrincipal.getAuthorities());

        usernamePasswordAuthenticationToken
                .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    @Override
    public String generateJwtToken(UserPrincipal userPrincipal) throws Exception {
        return Properties.TOKEN_PREFIX + tokenProvider.generateToken(userPrincipal);
    }

    @Override
    public void checkIsPasswordExpired(UserPrincipal principal) throws Exception {
//        if (principal.isPasswordExpired())
//            throw new PasswrodExpiredException();
    }

    @Override
    public void userIsVerified(UserPrincipal userPrincipal) throws Exception {
        try {
            User user = userPrincipal.getUser();
            if (!user.getRegisterStateEnum().name().equals(RegisterStateEnum.PASSWORD_SET.name())){
                Role role = user.getUserRolesSet().stream().findFirst().get();
                if (!role.getRoleName().equals("UNVERIFIED_USER"))
                    throw new AccessDeniedException("Account not verified yet");
            }
        }catch (Exception e){
            throw new AccessDeniedException("Account not verified yet");
        }
    }
}
