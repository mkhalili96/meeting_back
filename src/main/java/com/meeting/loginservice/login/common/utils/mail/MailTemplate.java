package com.meeting.loginservice.login.common.utils.mail;

import com.meeting.loginservice.login.model.entity.VerificationCode;

public class MailTemplate {


    public String generateVerificationMail(VerificationCode verificationCode) {

        return String.join(
                System.getProperty("line.separator"),
                "<h1>Your Code Is : "  + verificationCode.getDigit1() + " - "
                                                + verificationCode.getDigit2() + " - "
                                                + verificationCode.getDigit3() + " - "
                                                + verificationCode.getDigit4() + "</h1>"
        );
    }
}
