package com.meeting.loginservice.login.common.aspect;

import com.meeting.loginservice.login.model.entity.LoginMethodEnum;
import com.meeting.loginservice.login.model.entity.RegisterStateEnum;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Aspect
public class AspectiveNodeAuthorization {
    @Around(value = "@annotation(com.meeting.loginservice.login.common.aspect.GiveAccessTo)")
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        String role = ((MethodSignature) joinPoint.getSignature())
                .getMethod()
                .getAnnotation(GiveAccessTo.class)
                .role();

        User user = userPrincipal.getUser();
        List<String> roles = userPrincipal.getAuthorities().stream().map(grantedAuthority -> grantedAuthority.getAuthority()).collect(Collectors.toList());

//        if (user.getUserName() == null & role != "UNVERIFIED_USER")
//            throw new AccessDeniedException( "Access is denied");
        if (roles.contains(role) ){
            return joinPoint.proceed();
        }
        else{
            throw new AccessDeniedException( "Access is denied");
        }


    }
}