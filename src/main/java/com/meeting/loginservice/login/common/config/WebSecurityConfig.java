package com.meeting.loginservice.login.common.config;

import com.meeting.loginservice.login.model.entity.*;
import com.meeting.loginservice.login.model.repository.RoleRepository;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.model.serivce.userdetail.MeetingUserDetailsService;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.reactive.function.client.WebClient;
import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MeetingUserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }



    @Bean
    public JwtAuthorizationFilter jwtAuthorizationFilter() throws Exception {
        return new JwtAuthorizationFilter(authenticationManager());
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(9);
    }

    @Bean
    public JwtTokenProvider jwtTokenProvider(){
        return new JwtTokenProvider();
    }

    @Bean
    public WebClient webClient(){
        return WebClient.create();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)     //no cookies
                .and()
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/**" , "/api/user/register/mail/**" , "/api/password/reset-request" , "/api/user/exist" , "/api/file/profile/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(jwtAuthorizationFilter());
    }



    @PostConstruct
    void initalUserAndRoles() {

        try {
            Set<Role> roles = new HashSet<>();
            Role role = roleRepository.save(new Role("USER"));
            roleRepository.save(new Role("UNVERIFIED_USER"));
            roles.add(role);
            final User user = new User(null , "msina" , "smkhalili96@gmail.com" , "9120733413", CountryCodeEnum.IR,true , "P@$$w0rld" , roles , LoginMethodEnum.NATIVE , RegisterStateEnum.PASSWORD_SET , "sina" ,null,null, new Date(1996,2,22));
            userRepository.save(user);

            userRepository.findAll().stream().forEach(user1 -> System.out.println(user.getEmail() + " " + user.getPassword()));
        }catch ( Exception e ){
        }


    }

}
