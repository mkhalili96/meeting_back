package com.meeting.loginservice.login.common.exception.dto;

import com.sun.istack.Nullable;
import org.springframework.http.HttpStatus;

public enum ErrorMap {

    //NOT_FOUND code start with 4040
    USER_NOT_FOUND(404001),



    Unauthorized(401),
    INVALID_CREDENTIALS(401001),

    //FORBIDDEN403
    ACCESS_DENIED(403),
    FORBIDDEN(403),
    INVALID_VERIFICATION_REQUEST(403001),
    TOO_MANY_RETRIES(403002),
    WRONG_API_CALL(403003),



    //ALREADY code start with 4001
    BAD_REQUEST(400),
    VERSION_NOT_MATCH(400001),
    ALREADY_EXIST(400002),
    INPUT_VALIDATION_FAILED(400003),





    //406 Not Acceptable
    NOT_ACCEPTABLE(406),
    INCORRECT_VALUE(406001),
    RELATIONSHIP_EXCEPTION(406002),

    INTERNAL_SERVER_ERROR(500),

    ERROR(000);


    private final int value;

    private ErrorMap(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }

    public String toString() {
        return this.value + ":" + this.name();
    }

    public static HttpStatus getHttpStatus(int statusCode) {
        String statusCodestr = String.valueOf(statusCode);
        return HttpStatus.valueOf(Integer.parseInt(statusCodestr.substring(0, 3)));
    }

    public static ErrorMap valueOf(int statusCode) {
        ErrorMap ErrorsMap = resolve(statusCode);
        if (ErrorsMap == null) {
            throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
        } else {
            return ErrorsMap;
        }
    }

    @Nullable
    public static ErrorMap resolve(int statusCode) {
        ErrorMap[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            ErrorMap ErrorsMap = var1[var3];
            if (ErrorsMap.value == statusCode) {
                return ErrorsMap;
            }
        }
        return null;
    }
}
