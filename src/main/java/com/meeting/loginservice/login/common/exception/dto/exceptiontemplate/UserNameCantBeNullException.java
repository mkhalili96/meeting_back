package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class UserNameCantBeNullException extends ExceptionsTemplate {


    public UserNameCantBeNullException() {
        super("username cannot be null");
    }

    @Override
    public int getStatus() {
        return ErrorMap.NOT_ACCEPTABLE.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.NOT_ACCEPTABLE;
    }
}
