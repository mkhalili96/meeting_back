package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class ToManyVerificationRetriesException extends ExceptionsTemplate {


    public ToManyVerificationRetriesException() {
        super("too many retries, we sent you a new verification code");
    }

    @Override
    public int getStatus() {
        return ErrorMap.TOO_MANY_RETRIES.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.TOO_MANY_RETRIES;
    }
}
