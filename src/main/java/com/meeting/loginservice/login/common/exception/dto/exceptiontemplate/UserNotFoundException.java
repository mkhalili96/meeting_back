package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class UserNotFoundException extends ExceptionsTemplate {


    public UserNotFoundException() {
        super("user not found");
    }

    @Override
    public int getStatus() {
        return ErrorMap.USER_NOT_FOUND.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.USER_NOT_FOUND;
    }
}
