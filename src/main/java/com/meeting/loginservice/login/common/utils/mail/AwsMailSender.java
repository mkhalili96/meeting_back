package com.meeting.loginservice.login.common.utils.mail;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Service
public class AwsMailSender {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(AwsMailSender.class);

    final String FROM = "no-reply@itslinkup.com";
    final String FROMNAME = "LINK-UP";
    static final String SMTP_USERNAME = "AKIA3I4F46LUQMNRMVXF";
    static final String SMTP_PASSWORD = "BL6EZxwiFbf6zf4n3h8XztR1iPtqmVH45EkjniDx7ihW";
    static final String HOST = "email-smtp.ca-central-1.amazonaws.com";
    static final int PORT = 587;


    public void send(String to , String message , String subject) throws UnsupportedEncodingException, MessagingException {
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM,FROMNAME));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
        msg.setSubject(subject);
        msg.setContent(message,"text/html");
        Transport transport = session.getTransport();

        try
        {
            logger.info("Sending mail to "+ to);
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            logger.info("Email sent to "+ to);
        }
        catch (Exception ex) {
            logger.error("failed to sent mail to "+to + " - Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();
        }
    }

}
