package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class AlreadyExistException extends ExceptionsTemplate {


    public AlreadyExistException(String key) {
        super(key + " already exists");
    }

    @Override
    public int getStatus() {
        return ErrorMap.Unauthorized.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.Unauthorized;
    }
}
