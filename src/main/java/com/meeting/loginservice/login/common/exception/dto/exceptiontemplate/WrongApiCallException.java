package com.meeting.loginservice.login.common.exception.dto.exceptiontemplate;

import com.meeting.loginservice.login.common.exception.ExceptionsTemplate;
import com.meeting.loginservice.login.common.exception.dto.ErrorMap;

public class WrongApiCallException extends ExceptionsTemplate {


    public WrongApiCallException() {
        super("wrong api call");
    }

    @Override
    public int getStatus() {
        return ErrorMap.WRONG_API_CALL.value();
    }

    @Override
    public ErrorMap getError() {
        return ErrorMap.WRONG_API_CALL;
    }
}
