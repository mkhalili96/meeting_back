package com.meeting.loginservice.login.common.utils;

import org.springframework.stereotype.Service;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@Service
public class ImageConverter {

    public boolean convertFormat(InputStream inputStream , String outputImagePath , Format format){
        try {


            Image image = ImageIO.read(inputStream);

            BufferedImage bi = this.createResizedCopy(image, 512, 512, true);
            ImageIO.write(bi, "jpg", new File(outputImagePath));

        } catch (IOException e) {
            System.out.println("Error");
        }
        return true;
    }


    BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }

    public enum Format{
        JPEG,
        PNG,
        GIF
    }
}
