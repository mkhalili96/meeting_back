package com.meeting.loginservice.login.common.config;

public class Properties {
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String SECRET = "8f8s1a2d421a7-s2f311da4ds7b-47as2d2s41-95a314b6-fdf21405"; //can be generated from uuid
    public static final long EXPIRATION_TIME = 31536000000L;
//    public static final long EXPIRATION_TIME = 0;
    public static final String FACEBOOK_AUTH_URL = "https://graph.facebook.com/me?fields=email,first_name,last_name&access_token=%s";
}
