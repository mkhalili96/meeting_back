package com.meeting.loginservice.login.common.utils.mail;

public enum MailType {
    RESET_PASSWORD("LINK-UP Password Reset"),
    REGISTER("LINK-UP Email Verification"),
    RETRY("RETRY");

    private String header;

    MailType(String header) {
        this.header = header;
    }
}
