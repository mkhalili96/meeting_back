package com.meeting.loginservice.login.controller;


import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import com.meeting.loginservice.login.model.serivce.AuthService.AppleVerifier;
import com.meeting.loginservice.login.model.serivce.AuthService.NativeAuthenticator;
import com.meeting.loginservice.login.to.CredentialRequestModel;
import com.meeting.loginservice.login.to.TokenRequestModel;
import com.meeting.loginservice.login.model.serivce.AuthService.FacebookAuthenticator;
import com.meeting.loginservice.login.model.serivce.AuthService.GoogleAuthenticator;
import com.meeting.loginservice.login.common.utils.exception.InvalidTokenException;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger logger= LoggerFactory.getLogger(AuthController.class);


    @Autowired
    private FacebookAuthenticator facebookAuthenticator;

    @Autowired
    private GoogleAuthenticator googleAuthenticator;

    @Autowired
    NativeAuthenticator nativeAuthenticator;

    @Autowired
    AppleVerifier appleVerifier;

    @PostMapping("login/facebook")
    public ResponseEntity<?> facebook(@RequestBody @Valid TokenRequestModel tokenRequestModel , HttpServletRequest request) {
        logger.info("[FACEBOOK_LOGIN] called from ["+request.getRemoteAddr()+"]");
        return facebookAuthenticator.facebook(tokenRequestModel);
    }

    @PostMapping("login/google")
    public ResponseEntity<?> google(@RequestBody @Valid TokenRequestModel tokenRequestModel , HttpServletRequest request) throws GeneralSecurityException, IOException, InvalidTokenException, JSONException {
        logger.info("[GOOGLE_LOGIN] called from ["+request.getRemoteAddr()+"]");
        return googleAuthenticator.verify(tokenRequestModel.getAuthToken());
    }

    @PostMapping("login/apple")
    public ResponseEntity<?> apple(@RequestBody @Valid TokenRequestModel tokenRequestModel , HttpServletRequest request) throws Exception {
        logger.info("[APPLE_LOGIN] called from ["+request.getRemoteAddr()+"]");
        return appleVerifier.verify(tokenRequestModel.getAuthToken());
    }

    @PostMapping("login/native")
    public ResponseEntity<?> nativeLogin(@RequestBody @Valid CredentialRequestModel credentialRequestModel , HttpServletRequest request) throws Exception {
        logger.info("[NATIVE_LOGIN] called from ["+request.getRemoteAddr()+"] with username : ["+credentialRequestModel.getUniqueIdentity()+"]");
        return ResponseEntity.ok(nativeAuthenticator.authenticate(credentialRequestModel , request));
    }

}