package com.meeting.loginservice.login.controller;

import com.meeting.loginservice.login.common.aspect.GiveAccessTo;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.serivce.UserCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserCrudService userCrudService;


    @GetMapping(value = "/exist")
    public ResponseEntity<Map<String,Boolean>> userExist(@RequestParam String username)  {
        Map map = new HashMap();
        map.put("response" , userCrudService.userExist(username));
        return ResponseEntity.ok(map);
    }

    @GiveAccessTo(role = "USER")
    @GetMapping()
    public ResponseEntity<User> getUser(@RequestParam(required = false) String uid) throws Exception {
        if (uid == null){
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
            return ResponseEntity.ok(principal.getUser());
        }else {
            return ResponseEntity.ok(userCrudService.findUserByUid(uid));
        }
    }

    @GiveAccessTo(role = "USER")
    @GetMapping(value = "/all")
    public ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.ok(userCrudService.fidAll());
    }

}
