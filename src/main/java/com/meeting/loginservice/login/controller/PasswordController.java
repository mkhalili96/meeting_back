package com.meeting.loginservice.login.controller;

import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.serivce.UserRegisterService;
import com.meeting.loginservice.login.to.EmailTo;
import com.meeting.loginservice.login.to.PasswordTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/password")
public class PasswordController {

    @Autowired
    UserRegisterService userRegisterService;

    @PostMapping(value = "/reset-request")
    public ResponseEntity<Map> passwordResetRequest(@Valid @RequestBody EmailTo email) throws Exception {
        Map map = new HashMap();
        map.put("response" , "Email Sent Successfully");
        userRegisterService.passwordResetRequest(email);
        return ResponseEntity.ok(map);
    }

    @PreAuthorize("hasRole('USER')")
    @PatchMapping(value = "/reset")
    public ResponseEntity<Map> passwordReset(@Valid @RequestBody PasswordTo passwordTo) throws Exception {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        String userId = principal.getUser().getId();
        userRegisterService.passwordReset(passwordTo , userId);

        Map map = new HashMap();
        map.put("response" , "Password Reset Successfully");
        return ResponseEntity.ok(map);
    }


}
