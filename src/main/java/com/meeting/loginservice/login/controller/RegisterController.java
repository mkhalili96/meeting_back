package com.meeting.loginservice.login.controller;

import com.meeting.loginservice.login.common.aspect.GiveAccessTo;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.serivce.UserRegisterService;
import com.meeting.loginservice.login.to.EmailTo;
import com.meeting.loginservice.login.to.LoginResponse;
import com.meeting.loginservice.login.to.UserRegisterRequestTo;
import com.meeting.loginservice.login.to.VerificationCodeRequest;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user/register")
public class RegisterController {
    private Logger logger = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    UserRegisterService userRegisterService;

    @PostMapping(value = "/mail/send")
    public ResponseEntity<Map> sendConfirmationMail(@Valid @RequestBody EmailTo email) throws Exception {
        userRegisterService.sendConfirmationMail(email);
        Map map = new HashMap();
        map.put("response" , "Email Sent Successfully");
        return ResponseEntity.ok(map);
    }

    @PostMapping(value = "/mail/resend")
    public ResponseEntity<Map> resendVerificationCode(@Valid @RequestBody EmailTo email) throws Exception {
        userRegisterService.sendConfirmationMail(email);
        Map map = new HashMap();
        map.put("response" , "Email Sent Successfully");
        return ResponseEntity.ok(map);
    }

    @PostMapping(value = "/mail/confirm")
    public ResponseEntity<LoginResponse> confirmMail(@Valid @RequestBody VerificationCodeRequest verificationCodeRequest) throws Exception {
        return userRegisterService.confirmMail(verificationCodeRequest);
    }

    @GiveAccessTo(role = "UNVERIFIED_USER")
    @PatchMapping(value = "/info")
    public ResponseEntity<User> addUserInfo(@Valid @RequestBody UserRegisterRequestTo requestTo) throws Exception {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        return ResponseEntity.ok(userRegisterService.addUserInfoNative(user, requestTo));
    }

    @GiveAccessTo(role = "UNVERIFIED_USER")
    @PatchMapping(value = "/info/social")
    public ResponseEntity<User> addUserInfoSocial(@Valid @RequestBody UserRegisterRequestTo requestTo) throws Exception {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        return ResponseEntity.ok(userRegisterService.addUserInfoSocial(user, requestTo));
    }

    @GiveAccessTo(role = "USER")
    @PostMapping(value = "/profile-pic")
    public ResponseEntity<User> addProfilePic(@RequestParam("file") MultipartFile file) throws Exception {
        System.out.println("mimeType = " + file.getContentType() + " - JPEG = "+ContentType.IMAGE_JPEG.getMimeType());
//        if(!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_PNG.getMimeType(), ContentType.IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
//            throw new IllegalStateException("File must be an Image");
//        }

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        return ResponseEntity.ok(userRegisterService.addUserProfilePic(file , user));
    }


}
