package com.meeting.loginservice.login.model.serivce.AuthService;

import com.meeting.loginservice.login.common.config.Properties;
import com.meeting.loginservice.login.model.entity.*;
import com.meeting.loginservice.login.to.TokenRequestModel;
import com.meeting.loginservice.login.model.entity.facebook.FacebookUserModel;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.RoleRepository;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.to.LoginResponse;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class FacebookAuthenticator {

    private Logger logger = LoggerFactory.getLogger(FacebookAuthenticator.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private WebClient webClient;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Transactional
    public ResponseEntity<?> facebook(TokenRequestModel tokenRequestModel) {
        String templateUrl = String.format(Properties.FACEBOOK_AUTH_URL, tokenRequestModel.getAuthToken());

        FacebookUserModel facebookUserModel = webClient.get().uri(templateUrl).retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    throw new ResponseStatusException(clientResponse.statusCode(), "facebook login error");
                })
                .bodyToMono(FacebookUserModel.class)
                .block();
        logger.info("[FACEBOOK_LOGIN] token is valid");
        final Optional<User> userOptional = userRepository.findByExternalUniqueId(facebookUserModel.getId());

        if (userOptional.isEmpty()) {        //we have no user with given email so register them
            logger.info("[FACEBOOK_LOGIN] this is a new user from facebook with FacebookId : ["+facebookUserModel.getId()+"]");
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByRoleName("UNVERIFIED_USER").get());
            User user = new User(facebookUserModel.getId() ,null , facebookUserModel.getEmail() , null, null , false , new RandomString(40).toString()  , roles , LoginMethodEnum.FACEBOOK , RegisterStateEnum.VERIFIED , facebookUserModel.getFirstName() +"  "+ facebookUserModel.getLastName() ,null ,null, null);
            userRepository.save(user);

            final UserPrincipal userPrincipal = new UserPrincipal(user);

            String jwt = tokenProvider.generateToken(userPrincipal);

            logger.info("[FACEBOOK_LOGIN] new user added to database with id ["+user.getId()+"] and logged in successfully");
            return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
        } else { // user exists just login
            final User user = userOptional.get();
            logger.info("[FACEBOOK_LOGIN] user already have an account with id : ["+user.getId()+"]");
//            if ((user.getLoginMethodEnum() != LoginMethodEnum.FACEBOOK)) { //check if logged in with different logged in method
//                return ResponseEntity.badRequest().body("previously logged in with different login method");
//            }

            UserPrincipal userPrincipal = new UserPrincipal(user);


            String jwt = tokenProvider.generateTokenWithPrinciple(userPrincipal);
            logger.info("[FACEBOOK_LOGIN] user ["+user.getId()+"] logged in successfully");
            return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
        }
    }
}
