package com.meeting.loginservice.login.model.entity;

public enum RegisterStateEnum {
    NOT_VERIFIED,
    VERIFIED,
    PASSWORD_SET;
}
