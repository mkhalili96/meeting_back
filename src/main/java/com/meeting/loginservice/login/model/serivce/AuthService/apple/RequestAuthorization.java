package com.meeting.loginservice.login.model.serivce.AuthService.apple;

import com.meeting.loginservice.login.model.serivce.AuthService.AppleVerifier;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestAuthorization {
//    private Logger log = LoggerFactory.getLogger(RequestAuthorization.class);
//    private static String APPLE_AUTH_URL = "https://appleid.apple.com/auth/token";
//
///** Social Paramters DTO check here
// https://gist.github.com/balvinder294/8a6c8c4754c309a3a7052ed037bc3c3b
// Apple Id token payload DTO here
// https://gist.github.com/balvinder294/72a437791aab7c9708b5a74dcece41b9
// Token Response DTO here
// https://gist.github.com/balvinder294/344284456e06c37d5afcdf08b8381092
// *****************/
//    /****************************************/
//    public void authorizeApple(SocialParametersDTO socialParametersDTO) throws Exception {
//        log.debug("Get Apple User Profile {}", socialParametersDTO);
//        String appClientId = null;
//        if (socialParametersDTO.getIdentifierFromApp() != null) {
//            // if kid is sent from mobile app
//            appClientId = socialParametersDTO.getIdentifierFromApp();
//        } else {
//            // if doing sign in with web using predefined identifier
//            appClientId = appleClientId;
//        }
//        SocialUserDTO socialUserDTO = new SocialUserDTO();
//        // generate personal verification token
//        String token = generateJWT(appClientId);
//
//        ////////// Get OAuth Token from Apple by exchanging code
//        // Prepare client, you can use other Rest client library also
//        OkHttpClient okHttpClient = new OkHttpClient()
//                .newBuilder()
//                .connectTimeout(70, TimeUnit.SECONDS)
//                .writeTimeout(70, TimeUnit.SECONDS)
//                .readTimeout(70, TimeUnit.SECONDS)
//                .build();
//        // Request body for sending parameters as FormUrl Encoded
//        RequestBody requestBody = new FormBody
//                .Builder()
//                .add("client_id", appClientId)
//                .add("client_secret", token)
//                .add("grant_type", "authorization_code")
//                .add("code", socialParametersDTO.getAuthorizationCode())
//                .build();
//
//        // Prepare rest request
//        Request request = new Request
//                .Builder()
//                .url(APPLE_AUTH_URL)
//                .post(requestBody)
//                .header("Content-Type", "application/x-www-form-urlencoded")
//                .build();
//
//        // Execute api call and get Response
//        Response resp = okHttpClient.newCall(request).execute();
//        String response = resp.body().string();
//        // Parse response as DTO
//        ObjectMapper objectMapper = new ObjectMapper();
//        TokenResponse tokenResponse = objectMapper.readValue(response, TokenResponse.class);
//        // Parse id token from Token
//        String idToken = tokenResponse.getId_token();
//        String payload = idToken.split("\\.")[1];// 0 is header we ignore it for now
//        String decoded = new String(Decoders.BASE64.decode(payload));
//        AppleIDTokenPayload idTokenPayload = new Gson().fromJson(decoded, AppleIDTokenPayload.class);
//
//        // if we have user obj also from Web or mobile
//        // we get only at 1st authorization
//        if (socialParametersDTO.getUserObj() != null ) {
//            JSONObject user = new JSONObject(userObj);
//            JSONObject name = user.has("name") ? user.getJSONObject("name") : null;
//            String firstName = name.getString("firstName);
//                    String lastName = name.getString("lastName);
//        }
//
//        // Add your logic here
//    }
}
