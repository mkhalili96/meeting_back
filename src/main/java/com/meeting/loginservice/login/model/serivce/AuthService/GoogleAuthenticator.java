package com.meeting.loginservice.login.model.serivce.AuthService;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.JsonObject;
import com.meeting.loginservice.login.common.config.Properties;
import com.meeting.loginservice.login.model.entity.*;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.RoleRepository;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.to.LoginResponse;
import com.meeting.loginservice.login.common.utils.exception.InvalidTokenException;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.*;

@Component
public class GoogleAuthenticator {
    private static final HttpTransport transport = new NetHttpTransport();
    private static final JsonFactory jsonFactory = new GsonFactory();

    @Value("${google-client-id.android}")
    private static String ANDROID_CLIENT_ID;

    @Value("${google-client-id.ios}")
    private static String IOS_CLIENT_ID;

    private Logger logger = LoggerFactory.getLogger(FacebookAuthenticator.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    RoleRepository roleRepository;

    @Bean
    public RestTemplate getRestTemplate(){
        return  new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;


    @Transactional
    public ResponseEntity<?> verify(String idTokenString) throws GeneralSecurityException, IOException, InvalidTokenException, JSONException {

       GoogleUserModel  googleUserModel = verifyGoogleToken(idTokenString);
        logger.info("[GOOGLE_LOGIN] token is valid");

        final Optional<User> userOptional = userRepository.findByEmail(googleUserModel.getEmail());

        if (userOptional.isEmpty()) {        //we have no user with given email so register them
            logger.info("[GOOGLE_LOGIN] this is a new user from google with email : ["+googleUserModel.getEmail()+"]");
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByRoleName("UNVERIFIED_USER").get());
            final User user = new User(null ,null , googleUserModel.getEmail() , null , null , false , new RandomString(40).toString()  , roles , LoginMethodEnum.GOOGLE , RegisterStateEnum.VERIFIED , googleUserModel.getName() , null,null ,null);
            user.setUserRolesSet(roles);
            userRepository.save(user);

            final UserPrincipal userPrincipal = new UserPrincipal(user);

            String jwt = tokenProvider.generateToken(userPrincipal);


            logger.info("[GOOGLE_LOGIN] new user added to database with id ["+user.getId()+"] and logged in successfully");
            return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
        } else { // user exists just login
            final User user = userOptional.get();
            logger.info("[GOOGLE_LOGIN] user already have an account with id : ["+user.getId()+"]");
//            if ((user.getLoginMethodEnum() != LoginMethodEnum.FACEBOOK)) { //check if logged in with different logged in method
//                return ResponseEntity.badRequest().body("previously logged in with different login method");
//            }

            UserPrincipal userPrincipal = new UserPrincipal(user);


            String jwt = tokenProvider.generateTokenWithPrinciple(userPrincipal);
            logger.info("[GOOGLE_LOGIN] user ["+user.getId()+"] logged in successfully");
            return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
        }
    }

    private GoogleUserModel verifyGoogleToken(String idTokenString) throws IOException, InvalidTokenException, JSONException {
        GoogleIdToken googleIdToken = GoogleIdToken.parse(jsonFactory , idTokenString);
        GoogleIdToken.Payload payload = googleIdToken.getPayload();
        try {
            JSONObject json = new JSONObject(IOUtils.toString(new URL("https://oauth2.googleapis.com/tokeninfo?id_token="+idTokenString), Charset.forName("UTF-8")));
            try{
                if (json.get("error") != null | json.get("error_description") != null){
                    throw new InvalidTokenException("idToken is invalid");
                }
            }catch (JSONException e){
                logger.error(e.getMessage());
            }
        }catch (IOException e) {
            logger.error(e.getMessage());
            throw new InvalidTokenException("idToken is invalid");
        }

        GoogleUserModel userModel = new GoogleUserModel().toModel(payload);
        return userModel;
    }

//    private GoogleIdToken.Payload verifyToken(String idTokenString)
//            throws GeneralSecurityException, IOException, InvalidTokenException {
//
//        logger.info("[GOOGLE_LOGIN] verify token action started");
//
//        GoogleIdToken idToken = null;
//        try {
//            idToken = getVerifier().verify(idTokenString);
//        } catch (IllegalArgumentException e){
//            logger.info("[GOOGLE_LOGIN] token verification failed : "+e.getMessage());
//        }
//        System.out.println("idTokenString = "+ idTokenString);
//        System.out.println("token = "+ idToken);
//
//        if (idToken == null) {
//            logger.info("[GOOGLE_LOGIN] idToken is invalid (null)");
//            throw new InvalidTokenException("idToken is invalid");
//        }
//        logger.info("[GOOGLE_LOGIN] token verified successfully");
//        return idToken.getPayload();
//    }
//
//    private static GoogleIdTokenVerifier getVerifier(){
//        return  new GoogleIdTokenVerifier.
//                Builder(transport, jsonFactory)
//                .setIssuers(Arrays.asList("https://accounts.google.com", "accounts.google.com"))
//                .setAudience(Collections.singletonList("com.googleusercontent.apps.156693485617-g92iq8ehjdb16d4eti361koqqdu014ef"))
//                .build();
//    }

//    @PostConstruct
//    public void test() throws GeneralSecurityException, IOException {
//
//        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
//                // Specify the CLIENT_ID of the app that accesses the backend:
//                .setAudience(Collections.singletonList("com.googleusercontent.apps.156693485617-g92iq8ehjdb16d4eti361koqqdu014ef"))
//                // Or, if multiple clients access the backend:
//                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
//                .build();
//
//        System.out.println("expiretion = " + verifier.getExpirationTimeMilliseconds());
//
//// (Receive idTokenString by HTTPS POST)
//    String t = "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc3MjA5MTA0Y2NkODkwYTVhZWRkNjczM2UwMjUyZjU0ZTg4MmYxM2MiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIxNTY2OTM0ODU2MTctZzkyaXE4ZWhqZGIxNmQ0ZXRpMzYxa29xcWR1MDE0ZWYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIxNTY2OTM0ODU2MTctZzkyaXE4ZWhqZGIxNmQ0ZXRpMzYxa29xcWR1MDE0ZWYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDg2NDY2Mzc5NDE5NDgyOTc3NDAiLCJlbWFpbCI6InJlemFraG9uc2FyaTY5QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidEw3dExRak05Ql85YkVpZ09oUDNTQSIsIm5vbmNlIjoiZklmYWd6WnI1dmF5V1R6ZHdFM2laOURBc1llX29oMDJlMHdkTzdHa05kayIsIm5hbWUiOiJSZXphIEtob25zYXJpIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdnV2t2Ykg5bEpRVTI5YVl5WTcweFZ6clFvdC13MDdxaGo4VVJ3bzVnPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IlJlemEiLCJmYW1pbHlfbmFtZSI6Iktob25zYXJpIiwibG9jYWxlIjoiZW4iLCJpYXQiOjE2MjAxNTUwMjUsImV4cCI6MTYyMDE1ODYyNX0.fiqxZCvEq2NiwRMas05NLRI1j1VtzusHEeyNnMSVpXSU3y3jYH8D98ZqMIYXZtkWXh4COfFWn9cQO5ytjPyR290i6iWal1pXWQ8TARaEoqeTRZiWMdboevkdSW8agmot2ueW5X6OofutRRczy0G58a8ap9fNIwFQmnrgwY14Jfvji2g-7aAs2xNvLvFt3vcZaYmu6vHQ6yrK23h6P3i8lW9qjbUrdZ7BmkGEmdcLbEXF6yaq9vJBCqE8B23OvPHEdQXLgnipibxbtrtDv1BOu-jGPjUFqYZojiulTjX5X34ShyHGAdYO7MZzi_BdSUYng5XluiM8_kgE998d2oxwQw";
//        GoogleIdToken googleIdToken = GoogleIdToken.parse(jsonFactory , t);
//        System.out.println("tooooooo = "+googleIdToken.toString());
//
//        GoogleIdToken idToken = verifier.verify("eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc3MjA5MTA0Y2NkODkwYTVhZWRkNjczM2UwMjUyZjU0ZTg4MmYxM2MiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIxNTY2OTM0ODU2MTctZzkyaXE4ZWhqZGIxNmQ0ZXRpMzYxa29xcWR1MDE0ZWYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIxNTY2OTM0ODU2MTctZzkyaXE4ZWhqZGIxNmQ0ZXRpMzYxa29xcWR1MDE0ZWYuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDg2NDY2Mzc5NDE5NDgyOTc3NDAiLCJlbWFpbCI6InJlemFraG9uc2FyaTY5QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidEw3dExRak05Ql85YkVpZ09oUDNTQSIsIm5vbmNlIjoiZklmYWd6WnI1dmF5V1R6ZHdFM2laOURBc1llX29oMDJlMHdkTzdHa05kayIsIm5hbWUiOiJSZXphIEtob25zYXJpIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdnV2t2Ykg5bEpRVTI5YVl5WTcweFZ6clFvdC13MDdxaGo4VVJ3bzVnPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IlJlemEiLCJmYW1pbHlfbmFtZSI6Iktob25zYXJpIiwibG9jYWxlIjoiZW4iLCJpYXQiOjE2MjAxNTUwMjUsImV4cCI6MTYyMDE1ODYyNX0.fiqxZCvEq2NiwRMas05NLRI1j1VtzusHEeyNnMSVpXSU3y3jYH8D98ZqMIYXZtkWXh4COfFWn9cQO5ytjPyR290i6iWal1pXWQ8TARaEoqeTRZiWMdboevkdSW8agmot2ueW5X6OofutRRczy0G58a8ap9fNIwFQmnrgwY14Jfvji2g-7aAs2xNvLvFt3vcZaYmu6vHQ6yrK23h6P3i8lW9qjbUrdZ7BmkGEmdcLbEXF6yaq9vJBCqE8B23OvPHEdQXLgnipibxbtrtDv1BOu-jGPjUFqYZojiulTjX5X34ShyHGAdYO7MZzi_BdSUYng5XluiM8_kgE998d2oxwQw");
//        if (idToken != null) {
//            GoogleIdToken.Payload payload = idToken.getPayload();
//
//            // Print user identifier
//            String userId = payload.getSubject();
//            System.out.println("User ID: " + userId);
//
//            // Get profile information from payload
//            String email = payload.getEmail();
//            boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
//            String name = (String) payload.get("name");
//            String pictureUrl = (String) payload.get("picture");
//            String locale = (String) payload.get("locale");
//            String familyName = (String) payload.get("family_name");
//            String givenName = (String) payload.get("given_name");
//
//            // Use or store profile information
//            // ...
//
//        } else {
//            System.out.println("Invalid ID token.");
//        }
//    }



}
