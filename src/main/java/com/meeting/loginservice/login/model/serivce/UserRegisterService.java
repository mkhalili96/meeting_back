package com.meeting.loginservice.login.model.serivce;

import com.meeting.loginservice.filing.service.FileStorageService;
import com.meeting.loginservice.login.common.config.Properties;
import com.meeting.loginservice.login.common.exception.dto.exceptiontemplate.*;
import com.meeting.loginservice.login.common.utils.mail.AwsMailSender;
import com.meeting.loginservice.login.common.utils.mail.MailTemplate;
import com.meeting.loginservice.login.common.utils.mail.MailType;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import com.meeting.loginservice.login.model.entity.*;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.model.repository.VerificationCodeRepository;
import com.meeting.loginservice.login.to.*;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import static com.meeting.loginservice.login.model.entity.LoginMethodEnum.NATIVE;

@Service
public class UserRegisterService {

    private Logger logger = LoggerFactory.getLogger(UserRegisterService.class);


    @Autowired
    UserRepository userRepository;

    @Autowired
    AwsMailSender awsMailSender;

    @Autowired
    UserCrudService userCrudService;

    @Autowired
    VerificationCodeRepository verificationCodeRepository;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private FileStorageService fileStorageService;

    //NOT_VERIFIED
    @Transactional
    public void sendConfirmationMail(EmailTo email) throws Exception {
        logger.info("send confirmation code request for "+email.getEmail());
        Optional<User> userOptional = userRepository.findByEmail(email.getEmail());
        if (userOptional.isPresent()){
            logger.info("user already exist in database with RegisterState = "+userOptional.get().getRegisterStateEnum().name());
            if (userOptional.get().getRegisterStateEnum().equals(RegisterStateEnum.PASSWORD_SET))
                throw new InvalidVerificationRequest();
            else {
                User user =userOptional.get();
                if (user.getVerificationCode() == null){
                    VerificationCode verificationCode = verificationCodeRepository.save(new VerificationCode().generateNewVerificationCode());
                    user.setVerificationCode(verificationCode);
                }
                else
                    user.setVerificationCode(user.getVerificationCode().generateNewVerificationCode());
                user.setRegisterStateEnum(RegisterStateEnum.NOT_VERIFIED);
                Set<Role> roles = new HashSet<>();
                roles.add(userCrudService.getRole("UNVERIFIED_USER"));
                user.setUserRolesSet(roles);
                user.setLoginMethodEnum(NATIVE);
                user = userRepository.save(user);

                awsMailSender.send(email.getEmail()
                        , new MailTemplate().generateVerificationMail(user.getVerificationCode())
                        ,MailType.REGISTER.name());
            }
        }
        else {
            User user = new User();
            user.setRegisterStateEnum(RegisterStateEnum.NOT_VERIFIED);
            user.setEmail(email.getEmail());
            user.setPassword(new RandomString(40).toString());
            Set<Role> roles = new HashSet<>();
            roles.add(new Role("UNVERIFIED_USER"));
            user.setUserRolesSet(roles);
            user.setVerificationCode(new VerificationCode().generateNewVerificationCode());
            userCrudService.createMinimalUserBeforVerification(user);

            awsMailSender.send(email.getEmail()
                    , new MailTemplate().generateVerificationMail(user.getVerificationCode())
                    ,MailType.REGISTER.name());
        }
    }


    //VERIFIED
    @Transactional
    public ResponseEntity<LoginResponse> confirmMail(VerificationCodeRequest request) throws Exception {
        logger.info("confirmMail request for "+request.getEmail());

        User user = userRepository.findByEmail(request.getEmail()).orElseThrow(UserNotFoundException::new);//Email is not true
        if (user.getVerificationCode() == null)
            throw new WrongCodeException();
        VerificationCode code = user.getVerificationCode();

        if (code.getRetries() >= 4){
            logger.info("retries count = "+code.getRetries());
            user.setVerificationCode(user.getVerificationCode().generateNewVerificationCode());;
            user = userRepository.save(user);
            awsMailSender.send(user.getEmail()
                    , new MailTemplate().generateVerificationMail(user.getVerificationCode())
                    ,MailType.RETRY.name());
            throw new ToManyVerificationRetriesException();

        }else {
            if (code.getDigit1() == request.getDigit1() & code.getDigit2() == request.getDigit2() & code.getDigit3() == request.getDigit3() & code.getDigit4() == request.getDigit4()) {
                user.setRegisterStateEnum(RegisterStateEnum.VERIFIED);
                user.setVerificationCode(null);
                user.setLoginMethodEnum(NATIVE);
                user = userRepository.save(user);
                final UserPrincipal userPrincipal = new UserPrincipal(user);
                String jwt = tokenProvider.generateToken(userPrincipal);

                logger.info("user verified successfully with email "+user.getEmail());
                return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
            }else {
                logger.info("user verification failed for email "+user.getEmail());
                VerificationCode verificationCode =  user.getVerificationCode();
                verificationCode.setRetries(verificationCode.getRetries()+1);
                verificationCodeRepository.save(verificationCode);
                throw new WrongCodeException();
            }
        }
    }

    //PASSWORD_SET
    @Transactional
    public User addUserInfoNative(User user , UserRegisterRequestTo requestTo) throws Exception {
        if (requestTo.getUserName() == null)
            throw new UserNameCantBeNullException();
        user = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);

        System.out.println("username = "+user.getUserName() + " "+userRepository.existsByUserName(user.getUserName()));

        if (user.getUserName() != null && userRepository.existsByUserName(requestTo.getUserName()))
            throw new AlreadyExistException("Username");
        else if (user.getPhone() != null && userRepository.existsByPhoneAndPhoneVerified(requestTo.getPhone(), true))
            throw new AlreadyExistException("Phone number");
        else if (user.getLoginMethodEnum() != NATIVE )
            throw new WrongApiCallException();

        user = requestTo.toUser(user);
        Set<Role> roles = new HashSet<>();
        roles.add(userCrudService.getRole("USER"));
        user.setUserRolesSet(roles);
        user.setLoginMethodEnum(NATIVE);
        user.setRegisterStateEnum(RegisterStateEnum.PASSWORD_SET);

        user = userRepository.save(user);
        return user;
    }
    //PASSWORD_SET
    @Transactional
    public User addUserInfoSocial(User user , UserRegisterRequestTo requestTo) throws Exception {
        if (requestTo.getUserName() == null)
            throw new UserNameCantBeNullException();
        user = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);

        System.out.println("username = "+user.getUserName() + " "+userRepository.findById(user.getId()));

        if (user.getUserName() != null && userRepository.existsByUserName(requestTo.getUserName()))
            throw new AlreadyExistException("Username");
        else if (user.getPhone() != null && userRepository.existsByPhoneAndPhoneVerified(requestTo.getPhone(), true))
            throw new AlreadyExistException("Phone number");
        else if (user.getLoginMethodEnum() == NATIVE )
            throw new WrongApiCallException();

        user = requestTo.toUser(user);
        Set<Role> roles = new HashSet<>();
        roles.add(userCrudService.getRole("USER"));
        user.setUserRolesSet(roles);
        user.setRegisterStateEnum(RegisterStateEnum.PASSWORD_SET);
        user.setPassword(null);

        user = userRepository.save(user);
        return user;
    }


    //UPLOAD PHOTO
    public User addUserProfilePic(MultipartFile file , User loggedInUser) throws Exception {
        User user = userRepository.findById(loggedInUser.getId()).orElseThrow(UserNotFoundException::new);
        String fileName = fileStorageService.storeFile(file , UUID.randomUUID().toString()+".jpeg");
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file/profile/")
                .path(fileName)
                .toUriString();

        user.setProfilePicUrl(fileDownloadUri);

        user = userRepository.save(user);
        return user;
    }


    @Transactional
    public void passwordResetRequest(EmailTo email) throws Exception {
        logger.info("password reset code request for "+email.getEmail());
        User user = userRepository.findByEmail(email.getEmail()).orElseThrow(UserNotFoundException::new);//user not exist

        if (user.getVerificationCode() == null){
            VerificationCode verificationCode = verificationCodeRepository.save(new VerificationCode().generateNewVerificationCode());
            user.setVerificationCode(verificationCode);
        }
        else
            user.setVerificationCode(user.getVerificationCode().generateNewVerificationCode());

        user = userRepository.save(user);

        awsMailSender.send(email.getEmail()
                , new MailTemplate().generateVerificationMail(user.getVerificationCode())
                ,MailType.RESET_PASSWORD.name());

    }

    @Transactional
    public void passwordReset(PasswordTo passwordTo , String userId) throws Exception {
        logger.info("password reset request for "+userId);
        User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new); //user not found

        user.setPassword(passwordTo.getNewPassword());
        userRepository.save(user);

        logger.info("password reset successfully for "+userId);
    }

}
