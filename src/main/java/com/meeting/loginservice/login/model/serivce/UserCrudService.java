package com.meeting.loginservice.login.model.serivce;

import com.meeting.loginservice.login.common.exception.dto.exceptiontemplate.UserNotFoundException;
import com.meeting.loginservice.login.model.entity.Role;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.VerificationCode;
import com.meeting.loginservice.login.model.repository.RoleRepository;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.model.repository.VerificationCodeRepository;
import com.meeting.loginservice.login.to.EmailTo;
import com.meeting.loginservice.login.to.UserRegisterRequestTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserCrudService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    VerificationCodeRepository verificationCodeRepository;

    @Transactional
    public User createMinimalUserBeforVerification(User user){


        Set<Role> roles = new HashSet<>();
        for (Role role : user.getUserRolesSet()) {
            roles.add(getRole(role.getRoleName()));
        }
        user.setUserRolesSet(roles);

        if (user.getVerificationCode() != null)
            user.setVerificationCode(verificationCodeRepository.save(user.getVerificationCode()));


        return userRepository.save(user);
    }

    public Role getRole(String role){
        Optional<Role> roleOptional = roleRepository.findByRoleName(role);

        if (roleOptional.isPresent())
            return roleOptional.get();
        else
            return roleRepository.save(new Role(role));
    }



    public User findUserByUid( String uid ) throws Exception {
        return userRepository.findByEmailOrUserNameOrExternalUniqueId(uid).orElseThrow(UserNotFoundException::new);
    }

    public Boolean userExist(String username){
        return userRepository.existsByUserName(username);
    }

    public List<User> fidAll(){
        return userRepository.findAll();
    }
//    public User register( UserRegisterRequestTo userRegisterRequestTo ) throws Exception {
//        User user = userRegisterRequestTo.toUser();
//        Set<Role> roles = new HashSet<Role>();
//        roles.add(roleRepository.findByRoleName("USER").get());
//        user.setId(null);
//        user.setUserRolesSet(roles);
//        return userRepository.save(user);
//    }
//
//    public User update( UserRegisterRequestTo userRegisterRequestTo ) throws Exception {
//        User user = userRepository.findById(userRegisterRequestTo.getId()).orElseThrow(Exception::new);
//        user = userRegisterRequestTo.toUser(user);
//
//        return userRepository.save(user);
//    }
}
