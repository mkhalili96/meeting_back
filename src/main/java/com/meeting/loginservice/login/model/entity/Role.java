package com.meeting.loginservice.login.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Entity(name = "ROLE_TABLE")
@Table (name = "ROLE_TABLE")
@Data
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private Long id;

    @NonNull
    @Column( unique = true )
    private String roleName;

    @ToString.Exclude
    @ManyToMany(mappedBy = "userRolesSet", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<User> userEntities;


    public Role(@NonNull String roleName) {
        this.roleName = roleName;
    }

}
