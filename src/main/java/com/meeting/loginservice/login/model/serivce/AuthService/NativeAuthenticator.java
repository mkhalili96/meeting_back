package com.meeting.loginservice.login.model.serivce.AuthService;

import com.meeting.loginservice.login.common.utils.authbuilder.inf.AuthBuilder;
import com.meeting.loginservice.login.to.CredentialRequestModel;
import com.meeting.loginservice.login.to.LoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@Service
public class NativeAuthenticator implements Serializable {


    @Autowired
    AuthBuilder authBuilder;

    public LoginResponse authenticate(CredentialRequestModel credentialRequestModel , HttpServletRequest request) throws Exception {

        return authBuilder.doAuthentication(credentialRequestModel, request);
    }


}
