package com.meeting.loginservice.login.model.serivce.AuthService;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.meeting.loginservice.login.common.config.Properties;
import com.meeting.loginservice.login.common.utils.exception.InvalidTokenException;
import com.meeting.loginservice.login.common.utils.security.JwtTokenProvider;
import com.meeting.loginservice.login.model.entity.LoginMethodEnum;
import com.meeting.loginservice.login.model.entity.RegisterStateEnum;
import com.meeting.loginservice.login.model.entity.Role;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.RoleRepository;
import com.meeting.loginservice.login.model.repository.UserRepository;
import com.meeting.loginservice.login.to.IdTokenPayload;
import com.meeting.loginservice.login.to.LoginResponse;
import com.meeting.loginservice.login.to.TokenResponse;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import net.bytebuddy.utility.RandomString;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileReader;
import java.security.PrivateKey;
import java.sql.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class AppleVerifier {

    private Logger logger = LoggerFactory.getLogger(AppleVerifier.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Value("${apple-client-id}")
    String clientId;

    @Value("${apple-client-secret}")
    String clientSecret;

    public ResponseEntity<?> verify(String idToken) throws Exception {


//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//
//        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
//        map.add("code", idToken);
//        map.add("client_id", idToken);
//        map.add("client_secret", idToken);
//        map.add("grant_type", "authorization_code");
//
//        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//
//        JSONObject jsonObject = null;
//        try {
//            jsonObject = restTemplate.postForObject( "https://appleid.apple.com/auth/token", request , JSONObject.class );
//            if (jsonObject.get("error") != null){
//                System.out.printf("error = "+ jsonObject.get("error"));
//
//            }
//        }catch (RestClientException e){
//            System.out.println(e.getMessage());
//        }

        String auth = appleAuth(idToken ,false);

        System.out.println(auth);
        return null;
//        if (jsonObject != null){
//            final Optional<User> userOptional = userRepository.findByExternalUniqueId(jsonObject.getString("email"));
//
//            if (userOptional.isEmpty()) {        //we have no user with given email so register them
//                logger.info("[APPLE_LOGGIN] this is a new user from facebook with FacebookId : ["+jsonObject.getString("email")+"]");
//                Set<Role> roles = new HashSet<>();
//                roles.add(roleRepository.findByRoleName("UNVERIFIED_USER").get());
//                User user = new User(jsonObject.getString("email") ,null , jsonObject.getString("email") , null, null , false , new RandomString(40).toString()  , roles , LoginMethodEnum.APPLE , RegisterStateEnum.VERIFIED , jsonObject.getString("email") +"  "+ jsonObject.getString("email") ,null ,null, null);
//                userRepository.save(user);
//
//                final UserPrincipal userPrincipal = new UserPrincipal(user);
//
//                String jwt = tokenProvider.generateToken(userPrincipal);
//
//                logger.info("[APPLE_LOGGIN] new user added to database with id ["+user.getId()+"] and logged in successfully");
//                return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
//            } else { // user exists just login
//                final User user = userOptional.get();
//                logger.info("[APPLE_LOGGIN] user already have an account with id : ["+user.getId()+"]");
////            if ((user.getLoginMethodEnum() != LoginMethodEnum.FACEBOOK)) { //check if logged in with different logged in method
////                return ResponseEntity.badRequest().body("previously logged in with different login method");
////            }
//
//                UserPrincipal userPrincipal = new UserPrincipal(user);
//
//
//                String jwt = tokenProvider.generateTokenWithPrinciple(userPrincipal);
//                logger.info("[APPLE_LOGGIN] user ["+user.getId()+"] logged in successfully");
//                return ResponseEntity.ok(new LoginResponse(Properties.TOKEN_PREFIX + jwt));
//            }
//        }else
//            throw new InvalidTokenException("idToken is invalid");

    }


    //////////////////////

    private static String APPLE_AUTH_URL = "https://appleid.apple.com/auth/token";

    private static String KEY_ID = "DM5FWXCB49";
    private static String TEAM_ID = "C9DF8685U4";
    private static String CLIENT_ID = "com.itslinkup.linkup";
//    private static String WEB_CLIENT_ID = "com.your.bundle.id.web";
//    private static String WEB_REDIRECT_URL = "https://bundle.your.com/";

    private static PrivateKey pKey;

    private static PrivateKey getPrivateKey() throws Exception {
//read your key
//        String path = new ClassPathResource("apple/AuthKey_DM5FWXCB49.p8").getFile().getAbsolutePath();

        String path =  new File("apple/AuthKey_DM5FWXCB49.p8").getAbsolutePath();
        final PEMParser pemParser = new PEMParser(new FileReader(path));
        final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
        final PrivateKeyInfo object = (PrivateKeyInfo) pemParser.readObject();
        final PrivateKey pKey = converter.getPrivateKey(object);

        return pKey;
    }

    private static String generateJWT() throws Exception {
        if (pKey == null) {
            pKey = getPrivateKey();
        }

        String token = Jwts.builder()
                .setHeaderParam(JwsHeader.KEY_ID, KEY_ID)
                .setIssuer(TEAM_ID)
                .setAudience("https://appleid.apple.com")
                .setSubject(CLIENT_ID)
                .setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 5)))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(pKey, SignatureAlgorithm.ES256)
                .compact();
        System.out.println("generated jwt = "+token);
        return token;
    }

//    private static String generateWebJWT() throws Exception {
//        String token = Jwts.builder()
//                .setHeaderParam(JwsHeader.KEY_ID, KEY_ID)
//                .setIssuer(TEAM_ID)
//                .setAudience("https://appleid.apple.com")
//                .setSubject(WEB_CLIENT_ID)
//                .setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 5)))
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .signWith(getPrivateKey(), SignatureAlgorithm.ES256)
//                .compact();
//
//        return token;
//    }


    /*
     * Returns unique user id from apple
     * */
    public static String appleAuth(String authorizationCode, boolean forWeb) throws Exception {
        HttpResponse<String> response = Unirest.post(APPLE_AUTH_URL)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .field("client_id",  CLIENT_ID)
                .field("client_secret", generateJWT())
                .field("grant_type", "authorization_code")
                .field("code", authorizationCode)
//                .field("redirect_uri", forWeb ? WEB_REDIRECT_URL : null)
                .asString();

        System.out.println("res + " + response.getBody());
        TokenResponse tokenResponse=new Gson().fromJson(response.getBody(),TokenResponse.class);
        System.out.println("response = "+tokenResponse.toString());
        String idToken = tokenResponse.getId_token();
        String payload = idToken.split("\\.")[1];//0 is header we ignore it for now
        String decoded = new String(Decoders.BASE64.decode(payload));

        IdTokenPayload idTokenPayload = new Gson().fromJson(decoded,IdTokenPayload.class);
        System.out.println("email : "+idTokenPayload.getEmail());
        return idTokenPayload.getSub();
    }
}
