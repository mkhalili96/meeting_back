package com.meeting.loginservice.login.model.entity.principal;

import com.meeting.loginservice.login.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {
    private Logger logger = LoggerFactory.getLogger(UserPrincipal.class);

    private User mUser;
    private Set<SimpleGrantedAuthority> authorities = new HashSet<>();

    public UserPrincipal() {
    }

    public UserPrincipal(User user) {
        logger.info(user.getId() + " Logged-in");
        this.mUser = user;
        user.getUserRolesSet().stream().map(role -> role.getRoleName()).forEach(r -> {
            authorities.add(new SimpleGrantedAuthority(r));
        });
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        for (SimpleGrantedAuthority authority : authorities) {
            System.out.println(authority.getAuthority());
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return mUser.getPassword();
    }
    @Override
    public String getUsername() {
        return mUser.getId();
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser(){
        return mUser;
    }

    public Boolean isPasswordExpired(){
        return false ;
    }
}
