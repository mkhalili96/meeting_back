package com.meeting.loginservice.login.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Random;
@NoArgsConstructor
@Data
@Entity(name = "VerificationCode")
@Table (name = "VerificationCode")
public class VerificationCode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Integer digit1 = 0;

    @Column
    private Integer digit2 = 0;

    @Column
    private Integer digit3 = 0;

    @Column
    private Integer digit4 = 0;

    @Column
    private Integer retries = 0;

    @OneToOne
    User user;


    public VerificationCode generateNewVerificationCode(){
        Random random = new Random();
        this.digit1 = random.nextInt(10);
        this.digit2 = random.nextInt(10);
        this.digit3 = random.nextInt(10);
        this.digit4 = random.nextInt(10);
        this.retries = 0;
        return this;
    };


    @Override
    public String toString() {
        return "VerificationCode{" +
                "id=" + id +
                ", digit1=" + digit1 +
                ", digit2=" + digit2 +
                ", digit3=" + digit3 +
                ", digit4=" + digit4 +
                ", retries=" + retries +
                ", user=" + user +
                '}';
    }
}
