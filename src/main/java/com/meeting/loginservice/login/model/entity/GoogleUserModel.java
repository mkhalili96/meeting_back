package com.meeting.loginservice.login.model.entity;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import lombok.Data;

import java.util.Date;

@Data
public class GoogleUserModel {

    String clientId;

    String email;

    Boolean email_verified;

    Date exp;

    Date iat;

    String iss;

    String name;

    String picture;

    String given_name;

    String family_name;

    public GoogleUserModel toModel(GoogleIdToken.Payload payload){
        this.clientId = (String) payload.get("aud");
        this.email = payload.getEmail();
        this.email_verified = payload.getEmailVerified();
        this.exp = new Date((long)payload.get("exp")*1000);
//        this.iat = new Date((long)payload.get("iat")*1000);
        this.name = (String) payload.get("name");
        return this;
    }

}
