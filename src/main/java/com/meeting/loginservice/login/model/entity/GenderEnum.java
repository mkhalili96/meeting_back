package com.meeting.loginservice.login.model.entity;

public enum GenderEnum {
    FEMALE,
    MALE
}
