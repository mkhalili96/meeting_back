package com.meeting.loginservice.login.model.repository;


import com.meeting.loginservice.login.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByEmail(String email); //findBysmth where smth need to same as that of User field
    Optional<User> findById(String id);

    @Query("select t from USER_TABLE t where t.email = ?1 OR t.userName = ?1 OR t.phone = ?1 OR t.externalUniqueId = ?1")
    Optional<User> findByEmailOrUserNameOrExternalUniqueId(String input);
    Optional<User> findByExternalUniqueId(String userId);
    Boolean existsByUserName(String username);
    Boolean existsByPhoneAndPhoneVerified(String phone , Boolean verified);
    Boolean existsByUserNameOrPhone(String username , String phone);
    Optional<User> findByEmailOrExternalUniqueId(String email , String id);
}
