package com.meeting.loginservice.login.model.serivce.userdetail;

import com.meeting.loginservice.login.common.exception.dto.exceptiontemplate.UserNotFoundException;
import com.meeting.loginservice.login.model.entity.Role;
import com.meeting.loginservice.login.model.entity.User;
import com.meeting.loginservice.login.model.entity.principal.UserPrincipal;
import com.meeting.loginservice.login.model.repository.UserRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
@Slf4j
public class MeetingUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
        String[] userRoles = user.getUserRolesSet().stream().map(Role::getRoleName).toArray(String[]::new);
        log.info("User roles are (getAuthorities) : {}", userRoles);
        return AuthorityUtils.createAuthorityList(userRoles);
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String uniqueIdentity) throws UsernameNotFoundException {
        User user = userRepository.findByEmailOrUserNameOrExternalUniqueId(uniqueIdentity).get();
        if (user == null) {
            throw new UserNotFoundException();
        }

        log.info("User roles (findByUsername): {}", user.getUserRolesSet());

        return new UserPrincipal(user);
    }
}
