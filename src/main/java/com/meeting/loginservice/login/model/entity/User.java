package com.meeting.loginservice.login.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "USER_TABLE")
@Table (name = "USER_TABLE")
public class User implements Serializable {

    @Id
    @Column(name = "user_id" , unique = true , nullable = false)
    private String id;

    @Column(unique = true)
    private String externalUniqueId;

    @Column(nullable = true , unique = true)
    private String userName;

    @Email
    @Size(max = 40)
    @Column(nullable = true , unique = true)
    private String email;

    @OneToOne
    @JsonIgnore
    private VerificationCode verificationCode;

    @Column(nullable = true)
    private String phone;

    @Column
    private CountryCodeEnum countryCode;

    @Column
    private Boolean phoneVerified =false;

    @NotBlank
    @Size(max = 100)
    @NotNull
    @JsonIgnore
    private String password;


    @ToString.Exclude
    @ManyToMany(cascade = CascadeType.MERGE , fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "role_id")})
    private Set<Role> userRolesSet;

    @Enumerated(EnumType.STRING)
    private LoginMethodEnum loginMethodEnum;

    @Column
    @JsonIgnore
    private RegisterStateEnum registerStateEnum = RegisterStateEnum.NOT_VERIFIED;

    @Column
    private String name;

    @Column
    private GenderEnum gender;

    @Column
    private String profilePicUrl;


    @Column(nullable = true , unique = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;


    @PrePersist
    public void prePersist() {
        if (password == null)
            this.password = new BCryptPasswordEncoder(9).encode(new RandomString(40).toString());
        this.setId( UUID.randomUUID().toString() );
        if (this.password.length() <= 55) {
            this.password = new BCryptPasswordEncoder(9).encode(password);
        }

    }

    @PreUpdate
    public void preUpdate() {
        if (password == null)
            this.password = new BCryptPasswordEncoder(9).encode(new RandomString(40).toString());
        if (this.password.length() <= 55) {
            this.password = new BCryptPasswordEncoder(9).encode(password);
        }

    }

    public User(String externalUniqueId, String userName, @Email @Size(max = 40) String email, String phone, CountryCodeEnum countryCode, Boolean phoneVerified, @NotBlank @Size(max = 100) @NotNull String password, Set<Role> userRolesSet, LoginMethodEnum loginMethodEnum, RegisterStateEnum registerStateEnum, String name, GenderEnum gender, String profilePicUrl, Date birthDate) {
        this.externalUniqueId = externalUniqueId;
        this.userName = userName;
        this.email = email;
        this.phone = phone;
        this.countryCode = countryCode;
        this.phoneVerified = phoneVerified;
        this.password = password;
        this.userRolesSet = userRolesSet;
        this.loginMethodEnum = loginMethodEnum;
        this.registerStateEnum = registerStateEnum;
        this.name = name;
        this.gender = gender;
        this.profilePicUrl = profilePicUrl;
        this.birthDate = birthDate;
    }

}
