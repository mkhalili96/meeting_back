package com.meeting.loginservice.login.model.serivce.AuthService.apple;

import java.io.Serializable;

public class SocialParametersDTO implements Serializable {

    private static final long serialVersionUID = 3484800209656475818L;

    // code varaible returned from sign in request
    private String authorizationCode;


    // If Apple sign in authoriation sends user object as string
    private String userObj;

    // id token from Apple Sign in Authorization if asked
    private String idToken;

    // kid or key identifier from mobile app authorization
    private String identifierFromApp;


    public String getUserObj() {
        return userObj;
    }
    public void setUserObj(String userObj) {
        this.userObj = userObj;
    }
    public String getIdToken() {
        return idToken;
    }
    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
    public String getAuthorizationCode() {
        return authorizationCode;
    }
    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }
    public String getIdentifierFromApp() {
        return identifierFromApp;
    }
    public void setIdentifierFromApp(String identifierFromApp) {
        this.identifierFromApp = identifierFromApp;
    }
}