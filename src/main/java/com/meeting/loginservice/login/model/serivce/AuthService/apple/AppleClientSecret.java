package com.meeting.loginservice.login.model.serivce.AuthService.apple;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import io.jsonwebtoken.io.Decoders;
import okhttp3.*;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import java.io.FileReader;
import java.security.PrivateKey;
import java.security.PublicKey;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.io.File;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;

@Service
public class AppleClientSecret {

    private Logger log = LoggerFactory.getLogger(AppleClientSecret.class);

    private String generateJWT(String identiferFromApp) throws Exception {
        // Generate a private key for token verification from your end with your creds
        PrivateKey pKey = generatePrivateKey();
        String token = Jwts.builder()
                .setHeaderParam(JwsHeader.KEY_ID, "DM5FWXCB49")
                .setIssuer("C9DF8685U4")
                .setAudience("https://appleid.apple.com")
                .setSubject(identiferFromApp)
                .setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 50)))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(pKey, SignatureAlgorithm.ES256)
                .compact();
        System.out.println("token2 = "+token);
        return token;
    }

    // Method to generate private key from certificate you created
    private PrivateKey generatePrivateKey() throws Exception {
        // here i have added cert at resource/apple folder. So if you have added somewhere else, just replace it with your path ofcert
        File file = new File("apple/AuthKey_DM5FWXCB49.p8");
        final PEMParser pemParser = new PEMParser(new FileReader(file));
        final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
        final PrivateKeyInfo object = (PrivateKeyInfo) pemParser.readObject();
        final PrivateKey pKey = converter.getPrivateKey(object);
        pemParser.close();
        System.out.println("PrivateKey = "+pKey.toString());
        return pKey;
    }

    private static String APPLE_AUTH_URL = "https://appleid.apple.com/auth/token";

/** Social Paramters DTO check here
 https://gist.github.com/balvinder294/8a6c8c4754c309a3a7052ed037bc3c3b
 Apple Id token payload DTO here
 https://gist.github.com/balvinder294/72a437791aab7c9708b5a74dcece41b9
 Token Response DTO here
 https://gist.github.com/balvinder294/344284456e06c37d5afcdf08b8381092
 *****************/
    /************** **************************/
    public void authorizeApple(SocialParametersDTO socialParametersDTO) throws Exception {
//        log.debug("Get Apple User Profile {}", socialParametersDTO);
//        String appClientId = null;
//        if (socialParametersDTO.getIdentifierFromApp() != null) {
//            // if kid is sent from mobile app
//            appClientId = socialParametersDTO.getIdentifierFromApp();
//        } else {
//            // if doing sign in with web using predefined identifier
//            appClientId = "com.itslinkup.linkup";
//        }
//        SocialUserDTO socialUserDTO = new SocialUserDTO();
//        // generate personal verification token
//        String token = generateJWT(appClientId);
//
//        ////////// Get OAuth Token from Apple by exchanging code
//        // Prepare client, you can use other Rest client library also
//        OkHttpClient okHttpClient = new OkHttpClient()
//                .newBuilder()
//                .connectTimeout(70, TimeUnit.SECONDS)
//                .writeTimeout(70, TimeUnit.SECONDS)
//                .readTimeout(70, TimeUnit.SECONDS)
//                .build();
//        // Request body for sending parameters as FormUrl Encoded
//        RequestBody requestBody = new FormBody
//                .Builder()
//                .add("client_id", appClientId)
//                .add("client_secret", token)
//                .add("grant_type", "authorization_code")
//                .add("code", socialParametersDTO.getAuthorizationCode())
//                .build();
//
//        // Prepare rest request
//        Request request = new Request
//                .Builder()
//                .url(APPLE_AUTH_URL)
//                .post(requestBody)
//                .header("Content-Type", "application/x-www-form-urlencoded")
//                .build();
//
//        // Execute api call and get Response
//        Response resp = okHttpClient.newCall(request).execute();
//        String response = resp.body().string();
//        // Parse response as DTO
//        ObjectMapper objectMapper = new ObjectMapper();
//        TokenResponse tokenResponse = objectMapper.readValue(response, TokenResponse.class);
//        // Parse id token from Token
//        String idToken = tokenResponse.getId_token();
//        String payload = idToken.split("\\.")[1];// 0 is header we ignore it for now
//        String decoded = new String(Decoders.BASE64.decode(payload));
//        AppleIDTokenPayload idTokenPayload = new Gson().fromJson(decoded, AppleIDTokenPayload.class);
//
//        // if we have user obj also from Web or mobile
//        // we get only at 1st authorization
//        if (socialParametersDTO.getUserObj() != null ) {
//            JSONObject user = new JSONObject(userObj);
//            JSONObject name = user.has("name") ? user.getJSONObject("name") : null;
//            String firstName = name.getString("firstName");
//            String lastName = name.getString("lastName");
//        }

        // Add your logic here
    }

    @PostConstruct
    public void m() throws Exception {
        generateJWT("eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLml0c2xpbmt1cC5saW5rdXAiLCJleHAiOjE2MjA3NTU4NTEsImlhdCI6MTYyMDY2OTQ1MSwic3ViIjoiMDAxMDMwLjBkNjE5MDMwN2RlYTQyYThhNTQyYjZiMzc1MWM2ZmEwLjIzNTMiLCJjX2hhc2giOiI3QUh4QnVmZFFtMENSRnlUOU0zRGlRIiwiZW1haWwiOiJyZXphLmtob25zYXJpQHlhaG9vLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjoidHJ1ZSIsImF1dGhfdGltZSI6MTYyMDY2OTQ1MSwibm9uY2Vfc3VwcG9ydGVkIjp0cnVlfQ.c2CfvQGT6z7MJe8m_w9xRpAS3UMJKI93PoTt6UBB51DXnEKs0YEF64jGnWa4oyNnyF9a8dtt4-y0HrrHerslAu0AFUpTH9nTbX99OPFqxkmtUVeQDKJ9-LiumJA-WfvNdLFKtf_bXpPLGXDVlCJSeNfTJUoOZIZXmKn6E5y6rOB2TFRGV2asgOIlKbxKEAYy6v66cwGiUxcGQnRSkWbMgGEda5a3BdOtYhEnecM15liG-4bPZC5PWg1yukbxyjxR3K7hl2G20c6rTs6E6Mo6TSPX0NvqcQOYixjbA_JY-Oc3KBP332utI3qh-JSwfqY_9-DZ86LflkPoNWW0wzg9Nw");

    }
}
