package com.meeting.loginservice.login.model.entity;

public enum  LoginMethodEnum {
    NATIVE, FACEBOOK, GOOGLE, APPLE
}
