package com.meeting.loginservice.login.to;

import lombok.Data;
import javax.validation.constraints.Email;
import java.io.Serializable;

@Data
public class EmailTo implements Serializable {

    @Email
    private String email;

}
