package com.meeting.loginservice.login.to;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialRequestModel implements Serializable {
    private String uniqueIdentity;
    private String password;
}
