package com.meeting.loginservice.login.to;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class PasswordTo {
    private String oldPassword;
    @Pattern(regexp="^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\\d\\W])|(?=.*\\W)(?=.*\\d))|(?=.*\\W)(?=.*[A-Z])(?=.*\\d)).{5,}$",message="Invalid password")
    private String newPassword;
}
