package com.meeting.loginservice.login.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.meeting.loginservice.login.model.entity.LoginMethodEnum;
import com.meeting.loginservice.login.model.entity.User;
import lombok.Data;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

@Data
public class UserEditRequestTo implements Serializable {

    private String id;

    @Pattern(regexp="^(?=.{5,30}$)(?![_])(?!.*[_]{2})[a-zA-Z0-9_]+(?<![_])$",message="Invalid user name")
    private String userName;

    @Email
    @Size(max = 40)
    private String email;

    @Pattern(regexp="^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$",message="Invalid phone number")

    private String phone;

    @NotBlank
    @Size(max = 100)
    @NotNull
    @JsonIgnore
    @Pattern(regexp="^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\\d\\W])|(?=.*\\W)(?=.*\\d))|(?=.*\\W)(?=.*[A-Z])(?=.*\\d)).{5,}$",message="Invalid password")
    private String password;

    private String firstName;

    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;


}
