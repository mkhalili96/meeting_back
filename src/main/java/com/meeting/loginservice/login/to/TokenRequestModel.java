package com.meeting.loginservice.login.to;

import lombok.Data;

@Data
public class TokenRequestModel {
    private String authToken;
}
