package com.meeting.loginservice.login.to;

import lombok.Data;
import javax.persistence.Column;
import javax.validation.constraints.Email;

@Data
public class VerificationCodeRequest {

    @Email
    String email;

    @Column
    private Integer digit1;

    @Column
    private Integer digit2;

    @Column
    private Integer digit3;

    @Column
    private Integer digit4;

}
