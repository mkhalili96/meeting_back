package com.meeting.loginservice.login.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.meeting.loginservice.login.model.entity.CountryCodeEnum;
import com.meeting.loginservice.login.model.entity.GenderEnum;
import com.meeting.loginservice.login.model.entity.User;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
public class UserRegisterRequestTo implements Serializable {

    private String id;

    @Pattern(regexp="^(?=.{5,30}$)(?![_])(?!.*[_]{2})[a-zA-Z0-9_]+(?<![_])$",message="Invalid user name")
    private String userName;

    @Pattern(regexp="^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$",message="Invalid phone number")
    private String phone;

    private CountryCodeEnum countryCode;

    @NotBlank
    @Size(max = 48)
    @NotNull
    @Pattern(regexp="^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\\d\\W])|(?=.*\\W)(?=.*\\d))|(?=.*\\W)(?=.*[A-Z])(?=.*\\d)).{5,}$",message="Invalid password")
    private String password;

    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;

    GenderEnum gender;


    public UserRegisterRequestTo(String id, String userName, String phone, CountryCodeEnum countryCode, @NotBlank @Size(max = 48) @NotNull String password, String name, Date birthDate, GenderEnum gender) {
        this.id = id;
        this.userName = userName;
        this.phone = phone;
        this.countryCode = countryCode;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public User toUser(User user){
        user.setUserName(this.userName);
        user.setPhone(this.phone);
        user.setCountryCode(this.countryCode);
        user.setPassword(this.password);
        user.setName(this.name);
        user.setBirthDate(this.birthDate);
        user.setGender(this.gender);

        return user;
    }
}
