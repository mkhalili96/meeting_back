package com.meeting.loginservice;

import com.meeting.loginservice.filing.property.FileStorageProperties;
import com.meeting.loginservice.login.model.serivce.AuthService.GoogleAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
public class LoginServiceApplication {

    @Autowired
    GoogleAuthenticator googleAuthenticator;

    public static void main(String[] args) {
        SpringApplication.run(LoginServiceApplication.class, args);
    }

}
